package de.norisbrain.imagescaler.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@RestController
@CrossOrigin
public class ImageController {

    private Logger log = LoggerFactory.getLogger(ImageController.class);

    @Autowired
    ServletContext context;

    @PutMapping("/storeImage")
    public ResponseEntity<Void> test(@RequestParam("file") MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String contentType = file.getContentType();

        log.info("Got a file called '{}' of type '{}'", fileName, contentType);

        if (contentType == null || !contentType.startsWith("image/"))
        {
            log.error("Type '{}' is not an image type", contentType);
            return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build();
        }

        try {
            BufferedImage bimg = ImageIO.read(file.getInputStream());
            int width = bimg.getWidth();
            int height = bimg.getHeight();

            log.info("The uploaded image's dimensions (W x H) are {}x{}", width, height);
        } catch (IOException e) {
            log.error("A backend error occured while trying to read the image data", e);
            return ResponseEntity.internalServerError().build();
        }

        try {
            File uploadsFolder = new File("uploads");
            long timestamp = System.currentTimeMillis();
            String fullPath = uploadsFolder.getAbsolutePath() + "/" + timestamp + "_" + fileName;
            log.info("Saving the file to {}", fullPath);
            if (!uploadsFolder.exists())
            {
                log.info("Uploads folder does not exist, creating folder...");
                if (!uploadsFolder.mkdirs())
                {
                    log.error("Creating folder failed");
                    return ResponseEntity.internalServerError().build();
                }
            }
            file.transferTo(new File(fullPath));
            log.info("File was saved");
        } catch (IOException e) {
            log.error("An error occured in the backend while trying to save the file", e);
            return ResponseEntity.internalServerError().build();
        }

        return ResponseEntity.ok().build();
    }
}
