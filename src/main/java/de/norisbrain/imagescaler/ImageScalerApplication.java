package de.norisbrain.imagescaler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImageScalerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImageScalerApplication.class, args);
	}

}
